<?php
	function conectaDB() {
		$servidor = 'localhost';
		$dbnom = '';	//Nombre de la base de datos
		$usuario = '';	//Usuario para la base de datos
		$password = '';	//Contraseña de la DB
		$puerto = 3306;	//Puerto
		$conectID=new mysqli($servidor, $usuario, $password, $dbnom, $puerto);
		if(!$conectID->connect_error) {
			$conectID->set_charset('utf8');
			return $conectID;
		} else {
			return null;
		}
	}
?>
