<?php
const PRODUCCION = FALSE;

define("SECRETO_RECAPTCHA", "", false);
define("DOMINIO", "https://carpathialab.com", false);
define("MOVILAPP", "file:///", false);
define("SUBIDAS", "/var/tmp/", false);
define("PAG_DEFAULT", "./index.php", false);
define("CORREO_ORIGEN", "", FALSE);
define("CORREO_SERVIDOR", "", FALSE);
define("CORREO_USUARIO", "", FALSE);
define("CORREO_PASSWD", "", FALSE);
define("CORREO_PUERTO", "", FALSE);
define("NOMBRE_APLICACION", "Nueva Aplicación Hanumat", FALSE);
define("GOOGLE_MAPS_KEY", "", FALSE);

//TODO El ejército Cuadrumano es un arreglo con las posibles implementaciones de hanumat en diferentes servidores.
$ejercitoCuadrumano = array("./motores/hanumat.php");
?>
